import React from 'react';

const MealItem = (props) => {
    return(
        <li> this is a meal item of {props.quantity} ml</li>
    );
}

export default MealItem;