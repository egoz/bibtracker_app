import React, { Component } from 'react';

class MealAddForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            'quantity' : 0 ,
            'observations': ''
        };
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }


    onFormSubmit() {
        this.props.onAddMeal(this.state.quantity, this.state.observations);
        this.setState({
            'quantity' : 0 ,
            'observations': ''
        });
    }

    render() {
        return(
            <div>
                <form>
                    <label htmlFor='meal-quantity'>Quantity</label>
                    <input 
                        type='number' 
                        name='meal-quantity' 
                        value={this.state.quantity}
                        onChange={(event) => {
                            this.setState(
                                {'quantity':event.target.value }
                            );
                        }}
                    />
                    <label htmlFor='meal-observations'>Observations</label>
                    <input 
                        type='text' 
                        name='meal-observations' 
                        value={this.state.observations}
                        onChange={(event) => {
                            this.setState(
                                {'observations':event.target.value }
                            );
                        }}
                    />
                    <input type='button' value='Send !' onClick={this.onFormSubmit}/>
                </form>
            </div>
        );
    }
}

export default MealAddForm;