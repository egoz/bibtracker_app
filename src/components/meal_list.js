import React from 'react';
import MealItem from './meal_items';

const MealList = (props) => {
    const MealItems = props.meals.map((meal) => {
        return <MealItem key={meal.id}  quantity={meal.quantity} />
    });

    return(
        <ul>
            {MealItems}
        </ul>
    );
}

export default MealList;    