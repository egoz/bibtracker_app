const API_URL = 'http://localhost:5000/';

export default {    
    getJson: (url) => 
        fetch(API_URL + url)
            .then(response => {
                if (response.status !== 200) {
                    throw new Error(`Invalid response, HTTP ${response.status}`);
                }
                return response.json();
            })
        ,
    postJson: (url, body) => fetch(
        API_URL + url, 
        {
            method: 'POST', 
            body: JSON.stringify(body),
            headers: new Headers({'content-type': 'application/json'})
        })
        .then((response => {
            if (response.status !== 201) {
                throw new Error(`Invalid response, HTTP ${response.status}`);
            }            
            return response.json();
        }))
}