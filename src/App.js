import React, { Component } from 'react';
import './App.css';
import Utils from './utils';
import MealList from './components/meal_list';
import MealAddForm from './components/meal_add_form';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            'meals': []
        };

        this.getMeals = this.getMeals.bind(this);
        this.getMeals();        
    }

    addMeal(quantity, observations) {
        Utils.postJson(
            'bottle/add', 
            {
                'quantity' : quantity,
                'observations': observations
            }
        ).then(data => {
            this.getMeals();
            console.log('new meal: ' + data[0]);
        }).catch(error => {
            alert(error);
        });
    }

    getMeals() {
        Utils.getJson('bottle/get')            
            .then((data) => {
                this.setState({
                    'meals': data.bottles
                })
            }).catch(error => {
                alert(error);
            });
    }

    render() {
        return (
        <div className="App">
            <p> Hello React </p>
            <MealList meals={this.state.meals}/>
            <MealAddForm getMeals={this.getMeals} onAddMeal={this.addMeal}/>
        </div>
        );
    }
}

export default App;
